const express = require("express");

const controllers = require("../controllers/authentication.controllers");

const Router = express.Router();

Router.post("/generate", controllers.GenerateOTP);
Router.post("/check", controllers.CheckOTP);

module.exports = Router;