const User = require("../models/user.model");

const PayamSMS = require("payamsms-sdk");

require("dotenv").config();
env = process.env;

const sms = new PayamSMS(
    env.PAYAM_ORGANIZATION,
    env.PAYAM_USERNAME,
    env.PAYAM_PASSWORD,
    env.PAYAM_LINE
);

const makePasscode = (length) => {
    var result = '';
    var characters = '0123456789';
    var charactersLength = characters.length;

    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(
            Math.floor(
                Math.random() * charactersLength
            )
        );
    }

   return result;
}

const GenerateOTP = (req, res) => {
    const { phone } = req.body;

    const newCode = makePasscode(6);

    User.findOne({ phone })
        .then((result) => {
            if (result !== null) {   
                User.findOneAndUpdate({ phone }, { otp: newCode })
                    .then((newResult) => {
                        const messages = [
                            {
                                recipient: phone,
                                body: newCode,
                            },
                        ];
        
                        const send = sms.send(messages);
                        send.then((sent) => {
                            const message = sent[0];
            
                            if (message.code === 200) res.status(200).send({ message: "کد ارسال شد" });
                            else res.status(200).send({ message: "کد ارسال نشد" })
                        });
                    })
                    .catch((error) => res.status(500).send({ error }));
            } else {
                const newUser = new User({ phone, otp: newCode });

                newUser.save()
                    .then((newResult) => {
                        const messages = [
                            {
                                recipient: phone,
                                body: newCode,
                            },
                        ];
        
                        const send = sms.send(messages);
                        send.then((sent) => {
                            const message = sent[0];
            
                            if (message.code === 200) res.status(200).send({ message: "کد ارسال شد" });
                            else res.status(200).send({ message: "کد ارسال نشد" })
                        });
                    })
                    .catch((error) => res.status(500).send({ error }));
            }
        })
        .catch((error) => res.status(500).send({ error }));
}

const CheckOTP = (req, res) => {
    const { phone, passcode } = req.body;

    User.findOneAndUpdate({ phone, otp: passcode }, { otp: "" })
        .then((result) => {
            if (result !== null) res.status(200).send({ message: "کاربر وجود دارد", id: result.id });
            else res.status(500).send({ message: "کد وارد شده معتبر نیست" });
        })
        .catch((error) => res.status(500).send({ error }));
}

module.exports = {
    GenerateOTP,
    CheckOTP,
}