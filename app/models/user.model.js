const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userModel = new Schema(
    {
        otp: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        phone: {
            type: String,
            unique: true,
            required: true,
            default: "",
        },
    },
    {
        timestamps: true,
    }
);

const User = mongoose.model("user", userModel);

module.exports = User;